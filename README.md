# Meetup Software Crafts·wo·manship Rennes
# Lightning talks - 13 avril 2021

## Liste des talks proposés :

* **[Titre du talk]** - [Speaker(s)] - [Description rapide]

* **De `docker-compose` à `docker<space>compose`** - [Nicolas De Loof](https://www.linkedin.com/in/ndeloof) (Docker) - "compose" est l'un des outils préféré des utilisateurs de Docker, et pourtant en devenant employé j'ai découvert c'est aussi la bête noire de la compagnie. Petit retour sur deux ans passés pour faire du  "vilain petit canard" compose un "first-class citizen" directement intégré dans la command line Docker.

* **Des conteneurs dans mon navigateur** - [Benoît Masson](https://www.linkedin.com/in/benoitmasson/) (OVHcloud) - Présentation et démo de l'extension [multi-account containers](https://addons.mozilla.org/fr/firefox/addon/multi-account-containers/) de Firefox : cloisonnement des données, regroupement et masquage d'onglets, etc.

* **J'ai presque fini ! Pourquoi notre cerveau nous aide pas sur ce coup là...** - [David Laizé](https://www.linkedin.com/in/david-laize/) (SII) - Vous voyez ce dev qui chaque matin, sur la même User Story, dit invariablement : "J'ai presque fini ! Ce soir, c'est bon !!!". Je vous parle de deux ou trois biais qui amplifient le problème et une ou deux idées pour sortir du piège en équipe ;)

* **Les DSLs en Kotlin : la conception d'un code expressif, élégant et robuste** - [Corentin Leffy](https://www.linkedin.com/in/corentin-leffy/) (Développeur freelance) - Est-il vraiment possible d'écrire un code que tout le monde pourrait comprendre, même les personnes du métier ? Découvrons ensemble comme Kotlin et sa syntaxe nous permettent de nous rapprocher de cette douce utopie.
